import React from 'react';
import { Link } from 'react-router-dom';

import './BuscaInput.css';

function BuscaInput (props) {

  let exibir_busca_avancada = props.exibir_busca_avancada;

	return (
		<div className="Busca">
      <div className="BuscaInput">
        <img className="BuscaIcone" src="//placehold.it/25x25" alt="" />
        <input type="text" name="q" placeholder="Digite palavra-chave ou número da lei" />
      </div>
      {
        exibir_busca_avancada ? 
        <div className="LinksUteis BuscaAvancada">
          <Link className="LinkTema" to="/constituicao">Busca Avançada</Link>
        </div>
        :
        <div className="LinksUteis LinksDiretos">
          <Link to="/constituicao">Constituição</Link>
          <Link to="/codigos">Códigos</Link>
          <Link to="/estatutos">Estatutos</Link>
        </div>
      }
      
    </div>
	);
}

export default BuscaInput;