import React from 'react';
import './Footer.css';
import { Link } from 'react-router-dom';

function Footer() {
  return (
    <section className="Footer">
      <img className="LogoFooter" src="//placehold.it/50x50" alt="LogoFooter" />
      <Link to="/">
        Home
      </Link>
      <Link to="/institucional">
        Institucional
      </Link>
      <Link to="/privacy-policy">
        Política de Privacidade
      </Link>
      <Link to="/servicos">
        Serviços
      </Link>
      <Link to="/faq">
        FAQ
      </Link>
      <Link to="/Cidades">
        Cidades
      </Link>
      <Link to="/contato">
        Contato
      </Link>
      <p className="Disclaimer">
        Todos os direitos reservados - LeisFederais ®<br />Liz Serviços Online Ltda.
      </p>
    </section>
  );
}

export default Footer;
