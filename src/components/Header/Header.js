import React from 'react';
import './Header.css';

import { Link } from 'react-router-dom';

function Header() {
  return (
    <header className="Header">
      <Link to="/">
        <img className="Logo" src="//placehold.it/130x70" alt="Logo" />
      </Link>
      <p className="EscolherSite">
        <img className="Ico Ico-Trocar" src="//placehold.it/15x15" alt="" />
        Trocar
      </p>
      <div className="Menu">
        <img src="//placehold.it/25x25" alt="Menu" />
      </div>
    </header>
  );
}

export default Header;
