import React from 'react';
import { withRouter } from 'react-router-dom';

// componentes
import Header from '../Header/Header';
import Footer from '../Footer/Footer';
import Router from '../Router/Router';

import './Layout.css';
import ScrollToTop from './ScrollToTop';

function Layout (props) {
	return (
		<React.Fragment>
			<Header />
			<main className="Layout">
				<ScrollToTop />
				<Router {...props} />
			</main>
			<Footer />
		</React.Fragment>
	);
}

export default withRouter(Layout);
