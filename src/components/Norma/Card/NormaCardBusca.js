import React from 'react';
import './NormaCard.css';
import { Link } from 'react-router-dom';

function NormaCardBusca(props) {
    return (
        <Link to="/" className="NormaCard NormaCardBusca">
            <img className="NormaImagem" src="//placehold.it/120x160" alt="alt" />
            <p className="NormaEpigrafe">
                Decreto 46512/2020
            </p>
            <p className="NormaEmenta QuebraPalavra" lang="pt-br">
                Dispõe sobre o tempo máximo de espera para atendimento aos usuários do SUS...
            </p>
        </Link>
    );
}

export default NormaCardBusca;