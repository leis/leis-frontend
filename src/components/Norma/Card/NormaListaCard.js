import React from 'react';
import './NormaListaCard.css';
import NormaCard from './NormaCard';
import VejaMaisCard from '../../Norma/Card/VejaMaisCard';
import NormaCardBusca from './NormaCardBusca';

function NormaListaCard(props) {

    let busca = !!props.busca;

    if (busca) {
        return (
            <div className="NormaListaCard NormaListaBusca">
                <NormaCardBusca />
                <NormaCardBusca />
                <NormaCardBusca />
                <NormaCardBusca />
                <NormaCardBusca />
                <NormaCardBusca />
            </div>
        );
    } else {
        return (
            <div className="NormaListaCard">
                <NormaCard />
                <NormaCard />
                <NormaCard />
                <NormaCard />
                <NormaCard />
                <NormaCard />
                <VejaMaisCard />
            </div>
        );
    }
}

export default NormaListaCard;