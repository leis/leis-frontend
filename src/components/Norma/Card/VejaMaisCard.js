import React from 'react';
import './NormaCard.css';
import { Link } from 'react-router-dom';

function VejaMaisCard(props) {
    return (
        <Link to="/" className="NormaCard VejaMais">
            <img className="NormaImagem" src="//placehold.it/120x160" alt="alt" />
        </Link>
    );
}

export default VejaMaisCard;