import React from 'react';
import { Tab, Tabs, TabList, TabPanel } from 'react-tabs';

import NormaListaCard from '../../Norma/Card/NormaListaCard';

// import Breadcrumb from '../../../components/Breadcrumb/Breadcrumb';

import './BuscaResultados.css';
import BuscaInput from '../../Busca/BuscaInput';

function BuscaResultados () {
	return (
		<div className="BuscaResultados">
			{/*<Breadcrumb links={[{ text: 'Pagina não encontrada', to: '#'}]}/>*/}	
      <p className="ResultadosNumero TextoCentralizado">56 resultados da pesquisa por</p>
			<h3 className="TermoPesquisa TextoCentralizado">"Termo de Pesquisa"</h3>
      <BuscaInput exibir_busca_avancada={true} />
			<Tabs>
          <TabList>
            <Tab className="Pill">
              Todos
            </Tab>
            <Tab className="Pill">
              Constituição
            </Tab>
            <Tab className="Pill">
              Leis Ordinárias
            </Tab>
          </TabList>
            
          <TabPanel>
            <NormaListaCard busca={true}/>
          </TabPanel>

          <TabPanel>
            <NormaListaCard busca={true}/>
          </TabPanel>

          <TabPanel>
            <NormaListaCard busca={true}/>
          </TabPanel>
            
          </Tabs>
		</div>
	);
}

export default BuscaResultados;