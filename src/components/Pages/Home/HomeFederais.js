import React from 'react';
import { Link } from 'react-router-dom';
import { Tab, Tabs, TabList, TabPanel } from 'react-tabs';

import './Home.css';
import './HomeFederais.css';
import NormaListaCard from '../../Norma/Card/NormaListaCard';
import BuscaInput from '../../Busca/BuscaInput';


function HomeFederais() {
  return (
    <React.Fragment>
      <div className="Home HomeFederais">
        <section className="Cabecalho">
          <h1 className="Titulo TituloPrincipal">Mais de 4 milhões<br />de leis brasileiras disponíveis.</h1>
          <hr className="BarraCurta" />
          <BuscaInput />
        </section>
        <section className="Categorias">
          <h2 className="TituloSecao">Principais Categorias</h2>
          <Link to="/constituicao" className="CardLink Constituicao">
            <p>
              <strong>Constituição</strong><br />
              da República Federativa do Brasil
            </p>
          </Link>
          <Link to="/codigos" className="CardLink Codigos">
            <p>
              <strong>Códigos</strong>
            </p>
          </Link>
          <Link to="/estatutos" className="CardLink Estatutos">
            <p>
              <strong>Estatutos</strong>
            </p>
          </Link>
        </section>
        <section className="ListaLeisFederais FundoEscuro">
          <h2 className="TituloSecao">Leis Federais</h2>
          <Tabs>
            <TabList>
              <Tab className="Pill">
                Recentes
              </Tab>
              <Tab className="Pill">
                Populares
              </Tab>
              <Tab className="Pill">
                Mais Favoritas
              </Tab>
            </TabList>
            
            <TabPanel>
              <NormaListaCard versao_extendida={true} />
            </TabPanel>

            <TabPanel>
              <NormaListaCard versao_extendida={true} />
            </TabPanel>

            <TabPanel>
              <NormaListaCard versao_extendida={true} />
            </TabPanel>
            
          </Tabs>
        </section>
        <section className="CriarConta">
          <h2 className="TituloSecao TextoCentralizado">
            Crie uma conta para
          </h2>
          <hr className="BarraCurta BarraTema ElementoCentralizado" />
          <ul>
            <li>
              Realizar anotações nas leis;
            </li>
            <li>
              Guardar as leis que mais utiliza;
            </li>
            <li>
              Ser notificado quando determinada lei receber nova alteração;
            </li>
          </ul>
          <button className="Botao BotaoGrande BotaoInvertido">
            Crie uma conta
          </button>
        </section>
      </div>
    </React.Fragment>
  );
}

export default HomeFederais;