import React from 'react';

// import Breadcrumb from '../../../components/Breadcrumb/Breadcrumb';

import './Institucional.css';

function Institucional () {
	return (
		<div className="Institucional">
			{/*<Breadcrumb links={[{ text: 'Pagina não encontrada', to: '#'}]}/>*/}	
			<h2 className="TituloSecao TextoCentralizado">Institucional</h2>
			<hr className="BarraCurta ElementoCentralizado" />
			<p><strong>A Empresa</strong></p>
			<p>Consectetur adipiscing elit. Nam sodales, justo quis commodo condimentum, mi ligula pulvinar sem, id facilisis dolor orci ac diam. Maecenas quis velit sit amet dolor congue efficitur. Mauris varius et mi et finibus. Donec et felis et turpis ultricies finibus. Fusce eu sodales diam. Aenean erat nulla, scelerisque ut porttitor eget, vestibulum a magna. Nulla a lectus ornare, auctor dui vel, commodo risus.</p>
			<div className="Bandeira TextoCentralizado">
				<img src="//placehold.it/500x260" alt="Bandeira do Brasil" />
			</div>
			<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam sodales, justo quis commodo condimentum, mi ligula pulvinar sem, id facilisis dolor orci ac diam. Maecenas quis velit sit amet dolor congue efficitur. Mauris varius et mi et finibus. Donec et felis et turpis ultricies finibus. Fusce eu sodales diam. Aenean erat nulla, scelerisque ut porttitor eget, vestibulum a magna. Nulla a lectus ornare, auctor dui vel, commodo risus.</p>
		</div>
	);
}

export default Institucional;