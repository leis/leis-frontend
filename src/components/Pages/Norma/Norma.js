import React from 'react';
import { Link } from 'react-router-dom';

// import Breadcrumb from '../../../components/Breadcrumb/Breadcrumb';

import './Norma.css';

function Norma () {
	return (
		<div className="Norma">
			{/*<Breadcrumb links={[{ text: 'Pagina não encontrada', to: '#'}]}/>*/}	
			<h1 className="Epigrafe">Decreto No 20.585</h1>
			<h2 className="EpigrafeSubtitulo">16 de Dezembro de 2019</h2>
			<div className="Status">
				<span className="StatusNorma StatusEmVigor">Norma em vigor</span>
			</div>
			<div className="Relevante">
				<img className="RelevanteImagem" src="//placehold.it/320x80" alt="" />
				<p className="RelevanteNome">Código Municipal do Meio Ambiente</p>
			</div>
			<div className="Categorias">
				<Link to="/" className="Pill">
              		Todos
	            </Link>
	            <Link to="/" className="Pill">
              		Constituição
	            </Link>
	            <Link to="/" className="Pill">
              		Leis Ordinárias
	            </Link>
			</div>
			<h1 className="Ementa">Dispõe sobre a permissão de uso, a título precário e gratuito, à associação de saúde São Bento</h1>
			<p className="AtosVinculados">
				<button className="Botao BotaoGrande BotaoVinculados">
            		Atos Vinculados
          		</button>
			</p>
			<div className="TextoDaLei">
				<p className="Titulozinho">
					<strong>Texto da Lei</strong>
				</p>
				<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam sodales, justo quis commodo condimentum, mi ligula pulvinar sem, id facilisis dolor orci ac diam. Maecenas quis velit sit amet dolor congue efficitur. Mauris varius et mi et finibus. Donec et felis et turpis ultricies finibus. Fusce eu sodales diam. Aenean erat nulla, scelerisque ut porttitor eget, vestibulum a magna. Nulla a lectus ornare, auctor dui vel, commodo risus.</p>
				<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam sodales, justo quis commodo condimentum, mi ligula pulvinar sem, id facilisis dolor orci ac diam. Maecenas quis velit sit amet dolor congue efficitur. Mauris varius et mi et finibus. Donec et felis et turpis ultricies finibus. Fusce eu sodales diam. Aenean erat nulla, scelerisque ut porttitor eget, vestibulum a magna. Nulla a lectus ornare, auctor dui vel, commodo risus.</p>
				<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam sodales, justo quis commodo condimentum, mi ligula pulvinar sem, id facilisis dolor orci ac diam. Maecenas quis velit sit amet dolor congue efficitur. Mauris varius et mi et finibus. Donec et felis et turpis ultricies finibus. Fusce eu sodales diam. Aenean erat nulla, scelerisque ut porttitor eget, vestibulum a magna. Nulla a lectus ornare, auctor dui vel, commodo risus.</p>
			</div>
		</div>
	);
}

export default Norma;