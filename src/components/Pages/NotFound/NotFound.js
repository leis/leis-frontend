import React from 'react';

// import Breadcrumb from '../../../components/Breadcrumb/Breadcrumb';

import './NotFound.css';

function NotFound () {
	return (
		<React.Fragment>
			{/*<Breadcrumb links={[{ text: 'Pagina não encontrada', to: '#'}]}/>*/}
			<p>Pagina não encontrada</p>
		</React.Fragment>
	);
}

export default NotFound;