import React from 'react';
import { Route, Switch } from 'react-router-dom';
// import { connect } from 'react-redux';

// paginas
import HomeFederais from '../Pages/Home/HomeFederais';
import NotFound from '../Pages/NotFound/NotFound';
import Institucional from '../Pages/Institucional/Institucional';
import BuscaResultados from '../Pages/Busca/BuscaResultados';
import Norma from '../Pages/Norma/Norma';

// import Analytics from 'react-ga'

function Router (props) {
	/*componentDidMount() {
		Analytics.initialize('UA-127511965-1');
	}*/
	
	/*componentDidUpdate() {
		Analytics.pageview(window.location.pathname + window.location.search);
	}*/
	return (
		<Switch key={props.location}>
			<Route 
				exact path="/"
				component={HomeFederais}
			/>
			<Route 
				exact path="/institucional"
				component={Institucional}
			/>
			<Route 
				exact path="/privacy-policy"
				component={Institucional}
			/>
			<Route 
				exact path="/servicos"
				component={Institucional}
			/>
			<Route 
				exact path="/FAQ"
				component={Institucional}
			/>
			<Route 
				exact path="/busca"
				component={BuscaResultados}
			/>
			<Route 
				exact path="/lei"
				component={Norma}
			/>
			{/*<Route
				exact path="/:localSlug/:tipoSlug([\w\-]+)-:numero(\d+)-:ano(\d{4})"
				component={LeiPage}
			/>*/}
			{<Route
				component={NotFound} 
			/>}
		</Switch>
	);
}
/*
function mapStateToProps({ local }) {
	return { local };
}*/

// export default withRouter(connect(mapStateToProps, null)(Router));
export default Router;